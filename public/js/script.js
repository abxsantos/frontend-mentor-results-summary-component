
function createSummaryList(data) {
    const ul = document.querySelector('.summary-list');

    data.forEach(item => {
        const li = document.createElement('li');
        li.classList.add(item.category.toLowerCase());

        const typeDiv = document.createElement('div');
        typeDiv.classList.add('type');

        const img = document.createElement('img');
        img.src = item.icon;
        img.alt = `${item.category} icon`;

        const spanType = document.createElement('span');
        spanType.classList.add('summary-type');
        spanType.textContent = item.category;

        const scoreDiv = document.createElement('div');
        scoreDiv.classList.add('score');

        const spanScore = document.createElement('span');
        spanScore.classList.add('summary-score');
        spanScore.textContent = item.score;

        const spanCapScore = document.createElement('span');
        spanCapScore.classList.add('summary-cap-score');
        spanCapScore.textContent = ' / 100';

        typeDiv.appendChild(img);
        typeDiv.appendChild(spanType);

        scoreDiv.appendChild(spanScore);
        scoreDiv.appendChild(spanCapScore);

        li.appendChild(typeDiv);
        li.appendChild(scoreDiv);

        ul.appendChild(li);
    });
}

fetch('./data.json')
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.json();
    })
    .then(data => {
        // Call the function with fetched JSON data
        createSummaryList(data);
    })
    .catch(error => {
        console.error('There was a problem fetching the data:', error);
    });
